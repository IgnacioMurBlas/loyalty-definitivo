export const production: any = {
  postgres: {
    user: 'postgres',
    host: '127.0.0.1',
    database: 'at-loyalty-prod',
    password: '',
    port: 5432
  }
};

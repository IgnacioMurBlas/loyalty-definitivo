export const development: any = {
  postgres: {
    user: 'postgres',
    host: '127.0.0.1',
    database: 'DataBasePractica',
    password: 'zaragoza8',
    port: 5432
  },
  secrets: {
    session: 'xxx.hhhh.lllll',
    expiresIn: 60 * 60 * 24 * 30
  }
};

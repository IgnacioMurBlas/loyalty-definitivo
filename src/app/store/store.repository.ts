import { DatabaseService, DBQuery } from '../../database/src/index';
import { Service } from 'typedi';

import { Store } from './store.model';

@Service()
export class StoreRepository {
  constructor(private readonly databaseService: DatabaseService) {}

  async findAll(): Promise<Store[]> {
    const queryDoc: any = {
      sql:
        'SELECT s.*, a.email, a.password FROM public.store s inner join public.auth a on s.iduser = a.id',
      params: []
    };

    const stores = await this.databaseService.execQuery(queryDoc);
    return stores.rows;
  }

  async findById(storeId: number): Promise<Store> {
    const queryDoc: any = {
      sql:
        'SELECT s.*, a.email, a.password FROM public.store s inner join public.auth a on s.iduser = a.id WHERE s.id = $1',
      params: [storeId]
    };

    const stores = await this.databaseService.execQuery(queryDoc);
    return stores.rows[0];
  }

  async findByEmail(email: string): Promise<Store> {
    const queryDoc: any = {
      sql:
        'SELECT s.*, a.* FROM public.store s inner join public.auth a on s.iduser = a.id WHERE a.email = $1',
      params: [email]
    };

    const stores = await this.databaseService.execQuery(queryDoc);
    return stores.rows[0];
  }

  create(store: Store): DBQuery {
    return {
      sql:
        'INSERT INTO public.store (iduser, address) VALUES ($1, $2) RETURNING *',
      params: [store.iduser, store.address]
    };
  }
}

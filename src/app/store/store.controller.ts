import { Service } from 'typedi';
import { AuthService } from '../auth/auth.service';
import { Store } from './store.model';
import { StoreService } from './store.service';

@Service()
export class StoreController {
  constructor(
    private storeService: StoreService,
    private authService: AuthService
  ) {}

  /**
   * @api POST /stores
   *
   * This method creates a new store
   *
   * @param req
   * @param res
   * @param next
   */
  create(req: any, res: any, next: any) {
    if (req.body) {
      const store = req.body;

      this.authService
        .createStore(store)
        .then((newStore: Store) => {
          res.send(newStore);
        })
        .catch((error: Error) => {
          res.sendStatus(500);
        });
    }
  }

  /**
   * @api GET /stores
   *
   * @param req
   * @param res
   * @param next
   */
  getAll(req: any, res: any, next: any) {
    this.storeService
      .findAll()
      .then((storeList: Store[]) => {
        res.send(storeList);
      })
      .catch((error: Error) => {
        res.sendStatus(500);
      });
  }

  /**
   * @api GET /stores/:id
   *
   * @param req
   * @param res
   * @param next
   */
  getById(req: any, res: any, next: any) {
    if (req.params.id) {
      this.storeService
        .findById(parseInt(req.params.id))
        .then((store: Store | null) => {
          res.send(store);
        })
        .catch((error: Error) => {
          res.sendStatus(500);
        });
    }
  }
}

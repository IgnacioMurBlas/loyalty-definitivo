import { Service } from 'typedi';

import { StoreRepository } from './store.repository';
import { Store } from './store.model';
import { isNumber } from 'lodash';

@Service()
export class StoreService {
  constructor(private readonly storeRepository: StoreRepository) {}

  async findAll(): Promise<Store[]> {
    return await this.storeRepository.findAll();
  }

  async findById(storeId: number): Promise<Store> {
    if (!this.isValidId(storeId)) {
      return Promise.reject(new Error('InvalidStoreIdError'));
    }

    return await this.storeRepository.findById(storeId);
  }

  async findByEmail(email: string): Promise<Store> {
    return await this.storeRepository.findByEmail(email);
  }

  private isValidId(id: any): boolean {
    return id != null && isNumber(id) && id > 0;
  }
}

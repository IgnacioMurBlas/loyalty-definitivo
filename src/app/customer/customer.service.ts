import { Service } from 'typedi';

import { CustomerRepository } from './customer.repository';
import { Customer } from './customer.model';
import { isNumber } from 'lodash';

@Service()
export class CustomerService {
  constructor(private readonly customerRepository: CustomerRepository) {}

  async findAll(): Promise<Customer[]> {
    return await this.customerRepository.findAll();
  }

  async findById(customerId: number): Promise<Customer> {
    if (!this.isValidId(customerId)) {
      return Promise.reject(new Error('InvalidCustomerIdError'));
    }

    return await this.customerRepository.findById(customerId);
  }

  async findByEmail(email: string): Promise<Customer> {
    return await this.customerRepository.findByEmail(email);
  }

  private isValidId(id: any): boolean {
    return id != null && isNumber(id) && id > 0;
  }
}

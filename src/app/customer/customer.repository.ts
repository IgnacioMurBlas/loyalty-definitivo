import { DatabaseService, DBQuery } from '../../database/src/index';
import { Service } from 'typedi';

import { Customer } from './customer.model';

@Service()
export class CustomerRepository {
  constructor(private readonly databaseService: DatabaseService) {}

  async findAll(): Promise<Customer[]> {
    const queryDoc: any = {
      sql:
        'SELECT c.*, a.email, a.password FROM public.customer c inner join public.auth a on c.iduser = a.id',
      params: []
    };

    const customers = await this.databaseService.execQuery(queryDoc);
    return customers.rows;
  }

  async findById(customerId: number): Promise<Customer> {
    const queryDoc: any = {
      sql:
        'SELECT c.*, a.email, a.password FROM public.customer c inner join public.auth a on c.iduser = a.id WHERE c.id = $1',
      params: [customerId]
    };

    const customers = await this.databaseService.execQuery(queryDoc);
    return customers.rows[0];
  }

  async findByEmail(email: string): Promise<Customer> {
    const queryDoc: any = {
      sql:
        'SELECT c.*, a.* FROM public.customer c inner join public.auth a on c.iduser = a.id WHERE a.email = $1',
      params: [email]
    };

    const customers = await this.databaseService.execQuery(queryDoc);
    return customers.rows[0];
  }

  create(customer: Customer): DBQuery {
    return {
      sql:
        'INSERT INTO public.customer (iduser, birthdate) VALUES ($1, $2) RETURNING *',
      params: [customer.iduser, customer.birthdate]
    };
  }
}

import { Service } from 'typedi';
import { AuthService } from '../auth/auth.service';
import { Customer } from './customer.model';
import { CustomerService } from './customer.service';

@Service()
export class CustomerController {
  constructor(
    private customerService: CustomerService,
    private authService: AuthService
  ) {}

  /**
   * @api POST /customers
   *
   * This method creates a new customer
   *
   * @param req
   * @param res
   * @param next
   */
  create(req: any, res: any, next: any) {
    if (req.body) {
      const customer = req.body;

      this.authService
        .createCustomer(customer)
        .then((newCustomer: Customer) => {
          res.send(newCustomer);
        })
        .catch((error: Error) => {
          res.sendStatus(500);
        });
    }
  }

  /**
   * @api GET /customers
   *
   * @param req
   * @param res
   * @param next
   */
  getAll(req: any, res: any, next: any) {
    this.customerService
      .findAll()
      .then((customerList: Customer[]) => {
        res.send(customerList);
      })
      .catch((error: Error) => {
        res.sendStatus(500);
      });
  }

  /**
   * @api GET /customers/:id
   *
   * @param req
   * @param res
   * @param next
   */
  getById(req: any, res: any, next: any) {
    if (req.params.id) {
      this.customerService
        .findById(parseInt(req.params.id))
        .then((customer: Customer | null) => {
          res.send(customer);
        })
        .catch((error: Error) => {
          res.sendStatus(500);
        });
    }
  }
}

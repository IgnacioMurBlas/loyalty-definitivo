import { Service } from 'typedi';

import { AdminRepository } from './admin.repository';
import { Admin } from './admin.model';
import { isString, isNumber } from 'lodash';

@Service()
export class AdminService {
  constructor(private readonly adminRepository: AdminRepository) {}

  async findAll(): Promise<Admin[]> {
    return await this.adminRepository.findAll();
  }

  async findById(adminId: number): Promise<Admin> {
    if (!this.isValidId(adminId)) {
      return Promise.reject(new Error('InvalidAdminIdError'));
    }

    return await this.adminRepository.findById(adminId);
  }

  async findByEmail(email: string): Promise<Admin> {
    return await this.adminRepository.findByEmail(email);
  }

  private isValidId(id: any): boolean {
    return id != null && isNumber(id) && id > 0;
  }
}

import { Service } from 'typedi';
import { Admin } from './admin.model';
import { AdminService } from './admin.service';
import { AuthService } from '../auth/auth.service';

@Service()
export class AdminController {
  constructor(
    private adminService: AdminService,
    private authService: AuthService
  ) {}

  /**
   * @api POST /admins
   *
   * This method creates a new admin
   *
   * @param req
   * @param res
   * @param next
   */
  create(req: any, res: any, next: any) {
    if (req.body) {
      const admin = req.body;

      this.authService
        .createAdmin(admin)
        .then((newAdmin: Admin) => {
          res.send(newAdmin);
        })
        .catch((error: Error) => {
          res.sendStatus(500);
        });
    }
  }

  /**
   * @api GET /admins
   *
   * @param req
   * @param res
   * @param next
   */
  getAll(req: any, res: any, next: any) {
    this.adminService
      .findAll()
      .then((adminList: Admin[]) => {
        res.send(adminList);
      })
      .catch((error: Error) => {
        res.sendStatus(500);
      });
  }

  /**
   * @api GET /admins/:id
   *
   * @param req
   * @param res
   * @param next
   */
  getById(req: any, res: any, next: any) {
    if (req.params.id) {
      this.adminService
        .findById(parseInt(req.params.id))
        .then((admin: Admin | null) => {
          res.send(admin);
        })
        .catch((error: Error) => {
          res.sendStatus(500);
        });
    }
  }
}

import { DatabaseService, DBQuery } from '../../database/src/index';
import { Service } from 'typedi';

import { Admin } from './admin.model';

@Service()
export class AdminRepository {
  constructor(private readonly databaseService: DatabaseService) {}

  async findAll(): Promise<Admin[]> {
    const queryDoc: any = {
      sql:
        'SELECT ad.*, a.email, a.password FROM public.admin ad inner join public.auth a on ad.iduser = a.id',
      params: []
    };

    const admins = await this.databaseService.execQuery(queryDoc);
    return admins.rows;
  }

  async findById(adminId: number): Promise<Admin> {
    const queryDoc: any = {
      sql:
        'SELECT ad.*, a.email, a.password FROM public.admin c inner join public.auth ad on c.iduser = ad.id WHERE c.id = $1',
      params: [adminId]
    };

    const admins = await this.databaseService.execQuery(queryDoc);
    return admins.rows[0];
  }

  async findByEmail(email: string): Promise<Admin> {
    const queryDoc: any = {
      sql:
        'SELECT ad.*, a.* FROM public.admin ad inner join public.auth a on ad.iduser = a.id WHERE a.email = $1',
      params: [email]
    };

    const admins = await this.databaseService.execQuery(queryDoc);
    return admins.rows[0];
  }

  create(admin: Admin): DBQuery {
    return {
      sql:
        'INSERT INTO public.admin (iduser, name) VALUES ($1, $2) RETURNING *',
      params: [admin.iduser, admin.name]
    };
  }
}

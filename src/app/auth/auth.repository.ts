import {
  DatabaseService,
  DBQuery,
  DBQueryResult
} from '../../database/src/index';
import { Service } from 'typedi';

import { User } from './auth.model';
import { Customer } from '../customer/customer.model';
import { Admin } from '../admin/admin.model';
import { Store } from '../store/store.model';
import { StoreRepository } from '../store/store.repository';
import { AdminRepository } from '../admin/admin.repository';
import { CustomerRepository } from '../customer/customer.repository';

@Service()
export class AuthRepository {
  constructor(
    private readonly databaseService: DatabaseService,
    private readonly storeRepository: StoreRepository,
    private readonly adminRepository: AdminRepository,
    private readonly customerRepository: CustomerRepository
  ) {}

  create(user: User): DBQuery {
    return {
      sql:
        'INSERT INTO public.auth (email, password) VALUES ($1, $2) RETURNING *',
      params: [user.email, user.password]
    };
  }

  async createStore(store: Store): Promise<Store> {
    const transaction = await this.databaseService.startTransaction();

    const newUser: DBQueryResult = await transaction.addQuery(
      this.create(store)
    );

    store.iduser = newUser.rows[0].id;
    const newStore = await transaction.addQuery(
      this.storeRepository.create(store)
    );

    transaction.commit();

    return newStore.rows[0];
  }

  async createAdmin(admin: Admin): Promise<Admin> {
    const transaction = await this.databaseService.startTransaction();

    const newUser: DBQueryResult = await transaction.addQuery(
      this.create(admin)
    );
    admin.iduser = newUser.rows[0].id;
    const newAdmin = await transaction.addQuery(
      this.adminRepository.create(admin)
    );

    transaction.commit();

    return newAdmin.rows[0];
  }

  async createCustomer(customer: Customer): Promise<Customer> {
    const transaction = await this.databaseService.startTransaction();

    const newUser: DBQueryResult = await transaction.addQuery(
      this.create(customer)
    );
    customer.iduser = newUser.rows[0].id;
    const newCustomer = await transaction.addQuery(
      this.customerRepository.create(customer)
    );

    transaction.commit();

    return newCustomer.rows[0];
  }
}

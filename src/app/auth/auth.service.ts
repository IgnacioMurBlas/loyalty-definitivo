import { Service } from 'typedi';

import { AuthRepository } from './auth.repository';
import { Admin } from '../admin/admin.model';
import { Customer } from '../customer/customer.model';
import { Store } from '../store/store.model';
import { isString } from 'lodash';

@Service()
export class AuthService {
  constructor(private readonly authRepository: AuthRepository) {}

  async createAdmin(admin: Admin): Promise<Admin> {
    if (!this.isValidAdmin(admin)) {
      return Promise.reject(new Error('AdminInputValidationError'));
    }

    return await this.authRepository.createAdmin(admin);
  }

  async createCustomer(customer: Customer): Promise<Customer> {
    if (!this.isValidCustomer(customer)) {
      return Promise.reject(new Error('CustomerInputValidationError'));
    }

    return await this.authRepository.createCustomer(customer);
  }

  async createStore(store: Store): Promise<Store> {
    if (!this.isValidStore(store)) {
      return Promise.reject(new Error('StoreInputValidationError'));
    }

    return await this.authRepository.createStore(store);
  }

  private isValidAdmin(admin: Admin): boolean {
    return (
      admin != null &&
      isString(admin.name) &&
      admin.email != null &&
      admin.password != null
    );
  }

  private isValidCustomer(customer: Customer): boolean {
    return (
      customer != null && customer.email != null && customer.password != null
    );
  }

  private isValidStore(store: Store): boolean {
    return (
      store != null &&
      isString(store.address) &&
      store.email != null &&
      store.password != null
    );
  }
}

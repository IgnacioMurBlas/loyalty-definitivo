import { STATUS_CODES } from 'http';
import { StatusCodes } from 'http-status-codes';
import { Service } from 'typedi';
import { AuthJwtService } from '../../auth/auth-jwt.service';
import { Admin } from '../admin/admin.model';
import { AdminService } from '../admin/admin.service';
import { AuthService } from '../auth/auth.service';
import { Customer } from '../customer/customer.model';
import { CustomerService } from '../customer/customer.service';
import { Store } from '../store/store.model';
import { StoreService } from '../store/store.service';
import { User } from './auth.model';

@Service()
export class AuthController {
  constructor(
    private authJwtService: AuthJwtService,
    private authService: AuthService,
    private storeService: StoreService,
    private customerService: CustomerService,
    private adminService: AdminService
  ) {}

  /**
   * @api POST /customer/login
   *
   * This method logins a customer
   *
   * @param req
   * @param res
   * @param next
   */
  loginCustomer(req: any, res: any, next: any) {
    if (req.body) {
      const user = req.body;

      this.customerService
        .findByEmail(user.email)
        .then((customer: Customer) => {
          if (customer && customer.password === user.password) {
            const token: string = this.createUserToken(customer);
            res.send({ token: token });
          } else {
            return next(res.sendStatus(StatusCodes.UNAUTHORIZED));
          }
        })
        .catch((error: Error) => {
          res.sendStatus(StatusCodes.UNAUTHORIZED);
        });
    } else {
      return next(res.sendStatus(StatusCodes.BAD_REQUEST));
    }
  }

  /**
   * @api POST /customer/signup
   *
   * This method signsup a customer
   *
   * @param req
   * @param res
   * @param next
   */
  signupCustomer(req: any, res: any, next: any) {
    if (req.body) {
      const customer = req.body;

      this.authService
        .createCustomer(customer)
        .then((newCustomer: Customer) => {
          res.send({ id: newCustomer.id });
        })
        .catch((error: Error) => {
          res.sendStatus(StatusCodes.BAD_REQUEST);
        });
    } else {
      return next(res.sendStatus(StatusCodes.BAD_REQUEST));
    }
  }

  /**
   * @api POST /admin/login
   *
   * This method logins a admin
   *
   * @param req
   * @param res
   * @param next
   */
  loginAdmin(req: any, res: any, next: any) {
    if (req.body) {
      const user = req.body;

      this.adminService
        .findByEmail(user.email)
        .then((admin: Admin) => {
          if (admin && admin.password === user.password) {
            const token: string = this.createUserToken(admin);
            res.send({ token: token });
          } else {
            return next(res.sendStatus(StatusCodes.UNAUTHORIZED));
          }
        })
        .catch((error: Error) => {
          res.sendStatus(StatusCodes.UNAUTHORIZED);
        });
    } else {
      return next(res.sendStatus(StatusCodes.BAD_REQUEST));
    }
  }

  /**
   * @api POST /admin/signup
   *
   * This method signsup a customer
   *
   * @param req
   * @param res
   * @param next
   */
  signupAdmin(req: any, res: any, next: any) {
    if (req.body) {
      const admin = req.body;

      this.authService
        .createAdmin(admin)
        .then((newAdmin: Admin) => {
          res.send({ id: newAdmin.id });
        })
        .catch((error: Error) => {
          console.log(error);
          res.sendStatus(StatusCodes.BAD_REQUEST);
        });
    } else {
      return next(res.sendStatus(StatusCodes.BAD_REQUEST));
    }
  }

  /**
   * @api POST /store/login
   *
   * This method logins a store
   *
   * @param req
   * @param res
   * @param next
   */
  loginStore(req: any, res: any, next: any) {
    if (req.body) {
      const user = req.body;

      this.storeService
        .findByEmail(user.email)
        .then((store: Store) => {
          if (store && store.password === user.password) {
            const token: string = this.createUserToken(store);
            res.send({ token: token });
          } else {
            return next(res.sendStatus(StatusCodes.UNAUTHORIZED));
          }
        })
        .catch((error: Error) => {
          res.sendStatus(StatusCodes.UNAUTHORIZED);
        });
    } else {
      return next(res.sendStatus(StatusCodes.BAD_REQUEST));
    }
  }

  /**
   * @api POST /customer/signup
   *
   * This method signsup a customer
   *
   * @param req
   * @param res
   * @param next
   */
  signupStore(req: any, res: any, next: any) {
    if (req.body) {
      const store = req.body;

      this.authService
        .createStore(store)
        .then((newStore: Store) => {
          res.send({ id: newStore.id });
        })
        .catch((error: Error) => {
          res.sendStatus(StatusCodes.BAD_REQUEST);
        });
    } else {
      return next(res.sendStatus(StatusCodes.BAD_REQUEST));
    }
  }

  private createUserToken(user: User): string {
    return this.authJwtService.createToken(user);
  }
}

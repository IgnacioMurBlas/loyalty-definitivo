import { Router } from 'express';
import { Service } from 'typedi';
import { AuthController } from '../../app/auth/auth.controller';

@Service()
export class AuthApi {
  private apiAuthRouter: Router;

  constructor(private authController: AuthController) {
    this.initRouterAndSetApiAuthRoutes();
  }

  getApiAuthRouter(): Router {
    return this.apiAuthRouter;
  }

  private initRouterAndSetApiAuthRoutes(): void {
    this.apiAuthRouter = Router();

    this.setAuthRoutes();
  }

  private setAuthRoutes(): void {
    this.apiAuthRouter.post('/customer/login', (req, res, next) =>
      this.authController.loginCustomer(req, res, next)
    );

    this.apiAuthRouter.post('/customer/signup', (req, res, next) =>
      this.authController.signupCustomer(req, res, next)
    );

    this.apiAuthRouter.post('/admin/login', (req, res, next) =>
      this.authController.loginAdmin(req, res, next)
    );

    this.apiAuthRouter.post('/admin/signup', (req, res, next) =>
      this.authController.signupAdmin(req, res, next)
    );

    this.apiAuthRouter.post('/store/login', (req, res, next) =>
      this.authController.loginStore(req, res, next)
    );

    this.apiAuthRouter.post('/store/signup', (req, res, next) =>
      this.authController.signupStore(req, res, next)
    );
  }
}

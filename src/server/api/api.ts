import { Router } from 'express';
import { Service } from 'typedi';
import { AdminController } from '../../app/admin/admin.controller';
import { CustomerController } from '../../app/customer/customer.controller';
import { StoreController } from '../../app/store/store.controller';

@Service()
export class Api {
  private apiRouter: Router;

  constructor(
    private customerController: CustomerController,
    private storeController: StoreController,
    private adminController: AdminController
  ) {
    this.initRouterAndSetApiRoutes();
  }

  public getApiRouter(): Router {
    return this.apiRouter;
  }

  private initRouterAndSetApiRoutes(): void {
    this.apiRouter = Router();
    this.setRoutes();
  }

  private setRoutes(): void {
    this.apiRouter.get('/customers', (req, res, next) =>
      this.customerController.getAll(req, res, next)
    );

    this.apiRouter.get('/customers/:id', (req, res, next) =>
      this.customerController.getById(req, res, next)
    );

    this.apiRouter.get('/stores', (req, res, next) =>
      this.storeController.getAll(req, res, next)
    );

    this.apiRouter.get('/stores/:id', (req, res, next) =>
      this.storeController.getById(req, res, next)
    );

    this.apiRouter.get('/admins', (req, res, next) =>
      this.adminController.getAll(req, res, next)
    );

    this.apiRouter.get('/admins/:id', (req, res, next) =>
      this.adminController.getById(req, res, next)
    );
  }
}
